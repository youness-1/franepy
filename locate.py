import psycopg2

prov = ['da', 'di', 'del', 'della', 'de', 'nelle', 'in']

def app(word):
	for a in prov:
		if word == a:
			return True
	return False

def to_nome(word):
	Nword = ""
	for char in word:
		dc = ord(char)
		if (dc > 64 and dc < 91) or (dc > 96 and dc < 123):
			 Nword += char
	return Nword

def to_nome(word):
	Nword = ""
	for char in word:
		dc = ord(char)
		if (dc > 64 and dc < 91) or (dc > 96 and dc < 123):
			 Nword += char
	return Nword

def singleCheck(cursor, word):
	cursor.execute("SELECT * FROM comuni_ita WHERE comune like '"+word.lower()+"'")
	return cursor.fetchall()  # risultato singolo con coincidenza perfetta
		
def multiCheck(cursor, word):
	cursor.execute("SELECT * FROM comuni_ita WHERE comune like '"+word.lower()+"%'")
	return cursor.fetchall()  # risultato multiplo impreciso


def matchWords(text,connection):
	cursor = connection.cursor()
	ids = []
	mul_bkp = []
	word_bkp = []
	i = cont_nome = c = 0 # contatore per le parole

	words = text.split(' ')#splitta le parole
	len_words = len(words) #memorizza lunghezza delle
	
	#print "\033[33;40m --- \033[00m"
	
	while i < len_words:
		word = to_nome(words[i])

		if word.istitle(): # se la parola inizia con la maiuscola avvia la procedura	
		
			c = i # contatore parola corrente e prossime
			rec = ["","","",""] # lista parole

			# ricerca comuni simili alla parola passata
			mul_result = multiCheck(cursor, word)
			mul_bkp.append(mul_result)
			sin_result = singleCheck(cursor, word)
			mul = len(mul_result)
			sin = len(sin_result)
			
			
			
			# controllo il numero dei risultati e il conteggio delle iterazioni 
			cont_nome = 0 # azzero il contatore per la parola composta corrente e quelle registrate
			while mul != 0 and cont_nome < 3: # ci devono essere delle concidenze

				if mul == 1 and sin == 1: # passo il comune direttamente alla lista perchè esiste una sola coincidenza
					ids.append(mul_result)
					break	

				else: # coincidenze maggiori di 1

					# controllo se word è l'ultima parola del tweet
					if word == words[(len(words) - 1)]:
						word=singleCheck(cursor,word)
						#if not word:
						#	break							
						#else:
						ids.append(word)
						break	
					try:
						t_test = words[1+i]
					except IndexError:
						break

					# controllo se la prossima word appartiene a prov
					if app(words[1+i]):
						word += " " + words[1+i] +" "+ words[2+i] # unisco word con le due parole prossime
						if len(singleCheck(cursor, word)) == 1:
							ids.append(word)
							i += 2
							break
						break
					rec[cont_nome] = word # registro la parola corrente

					try:
						word_bkp.append(word)
						word += " " + words[1+c] # aggiungo alla parola corrente la parola successiva

					except IndexError:
						break

					mul_prev = mul_result[0][0]
					

					mul_result = multiCheck(cursor, word)
					sin_result = singleCheck(cursor, word)
					mul = len(mul_result)
					sin = len(sin_result)
					

					if mul == 0:
						if len(mul_prev) == 1:
							ids.append(mul_prev)
							break
					elif mul == 1 and word==mul_result[0][0]:
						try:
							ids.append(mul_result[0])
						except IndexError:
							ids.append(word)
						i += 1
						if word == "è":
							exit(2)
						break

				cont_nome += 1
				c += 1

		# aggiorno il contatore
		i += 1
	if not ids:
		for i in range(0,len(mul_bkp)):
			for j in range(0,len(mul_bkp[i])):
				for k in range(0,len(word_bkp)):
					if mul_bkp[i][j][0]==word_bkp[k-1].lower():
						ids.append(word_bkp[k-1])	
	return ids

	
	

